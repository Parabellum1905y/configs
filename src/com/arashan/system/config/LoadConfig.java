package com.arashan.system.config;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoadConfig {
	public static Map<String, Config> configs = new ConcurrentHashMap<String, Config>();
	private static final String OWN_CONFIG = "config.properties";
	
	private static Config ownConfig () {
		Config conf = null;
		if (configs.get(OWN_CONFIG) !=null){
			conf = configs.get(OWN_CONFIG);
		}else{
			try {
				conf = new Config (LoadConfig.class.getClassLoader().getResourceAsStream(OWN_CONFIG));
				configs.put(OWN_CONFIG, conf);
			} catch (IllegalArgumentException | ClassNotFoundException
					| IOException e) {
				System.err.printf ("error in loading :%s",e);
			}
		}
		return conf;
	}
	
	public static Config loadConfig (String name){
		Config ownConf  = ownConfig();
		String rootPath = ownConf.getProp("config.root")!=null?ownConf.getProp("config.root"):"";
		Config conf = null;
		if (configs.get(name) !=null){
			conf = configs.get(name);
		}else{
			try {
				if (rootPath.isEmpty()){
					conf = new Config (LoadConfig.class.getClassLoader().getResourceAsStream(name));
				}else {
					conf = new Config (rootPath+"/"+name);
				}
				configs.put(name, conf);
			} catch (IllegalArgumentException | ClassNotFoundException
					| IOException e) {
				System.err.printf ("error in loading :%s",e);
			}
		}
		return conf;
	}

	public static Config reloadConfig (String name){
		Config ownConf  = ownConfig();
		String rootPath = ownConf.getProp("config.root")!=null?ownConf.getProp("config.root"):"";
		Config conf = null;
		try {
			if (rootPath.isEmpty()){
				conf = new Config (LoadConfig.class.getClassLoader().getResourceAsStream(name));
			}else {
				conf = new Config (rootPath+"/"+name);
			}
			configs.put(name, conf);
		} catch (IllegalArgumentException | ClassNotFoundException
				| IOException e) {
			System.err.printf ("error in loading :%s",e);
		}
		return conf;
	}
}
